
/* 
  This a simple example of the aREST Library working with 
  the Arduino Yun. See the README file for more details.
 
  Written in 2014 by Marco Schwartz under a GPL license. 
*/

// Import required libraries
#include <Bridge.h>
#include <YunServer.h>
#include <YunClient.h>
#include <aREST.h>
#include <Servo.h>

#define FAN_PIN 5
#define SERVO_PIN 6
#define RED_PIN 3
#define GREEN_PIN 11
#define BLUE_PIN 13

// Create aREST instance
aREST rest = aREST();

// Yun Server
YunServer server(81);
Servo shadesServo;

// Variables to be exposed to the API
int motorSpeed = 0;

void setup(void)
{  
  shadesServo.attach(SERVO_PIN);
  shadesServo.write(90);
  pinMode(RED_PIN,OUTPUT);
  pinMode(GREEN_PIN,OUTPUT);
  pinMode(BLUE_PIN,OUTPUT);
  // Start Serial
  Serial.begin(115200);
  // Function to be exposed
  rest.function("setFanSpeed",setFanSpeed);
  rest.function("setServo",setServo);
  rest.function("setLightRed",setLightRed);
  rest.function("setLightGreen",setLightGreen);
  rest.function("setLightBlue",setLightBlue);
 // rest.function("setLight",setLight);
   
  // Give name and ID to device
  rest.set_id("001");
  rest.set_name("Actuator_Station");
   
  // Bridge startup
  Bridge.begin();

  // Listen for incoming connection only from localhost
  server.begin();
  pinMode(13,OUTPUT);
  pinMode(5,OUTPUT);
  digitalWrite(13,HIGH);
}

void loop() {
  YunClient client = server.accept();
  rest.handle(client);
}

int setLightRed(String command){
  //long sum = atoi(command.c_str());
  int sum =  command.toInt();
  Serial.println(sum);
  analogWrite(RED_PIN,sum);
  return sum;
}
int setLightGreen(String command){
  //long sum = atoi(command.c_str());
  int sum =  command.toInt();
  Serial.println(sum);
  analogWrite(GREEN_PIN,sum);
  return sum;
}
int setLightBlue(String command){
  //long sum = atoi(command.c_str());
  int sum =  command.toInt();
  Serial.println(sum);
  analogWrite(BLUE_PIN,sum);
  return sum;
}

/*int setLight(String command){
  long sum = atoi(command.c_str());
  int blue = sum & 11111111;
  Serial.print("Blue: ");
  Serial.println(blue);
  sum = sum >> 8;
  int green = sum & 11111111;
  Serial.print("Green: ");
  Serial.println(green);
  sum = sum >> 8;
  int red = sum & 11111111;
  Serial.print("Red: ");
  Serial.println(red);

  analogWrite(RED_PIN,red);
  analogWrite(GREEN_PIN,green);
  analogWrite(BLUE_PIN,blue);
  
  return red+green+blue;
}*/

int setServo(String command){
  int servoLocation = command.toInt();
  shadesServo.write(servoLocation);
  return servoLocation;
  
}

// Custom function accessible by the API
int setFanSpeed(String command) {
  motorSpeed = command.toInt();
  digitalWrite(FAN_PIN,HIGH);
  delay(500);
  analogWrite(FAN_PIN,motorSpeed);
  Serial.println("asdf");
  return motorSpeed;
}
