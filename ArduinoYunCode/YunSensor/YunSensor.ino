
/* 
  This a simple example of the aREST Library working with 
  the Arduino Yun. See the README file for more details.
 
  Written in 2014 by Marco Schwartz under a GPL license. 
*/

// Import required libraries
#include <Bridge.h>
#include <YunServer.h>
#include <YunClient.h>
#include <aREST.h>

#define LIGHT_PIN 12
#define ALARM_PIN 7
#define BUZZ_PIN 11
#define TEMP_PIN A0


// Create aREST instance
aREST rest = aREST();

// Yun Server
YunServer server(81);

// Variables to be exposed to the API
int motorSpeed = 0;
bool wasAlarm = false;
bool alarmEnable = true;

void alarm(){
  if(alarmEnable){
    wasAlarm = true;
    tone(BUZZ_PIN,500,500);
  }
}

void setup(void)
{  
  pinMode(LIGHT_PIN,INPUT);
  pinMode(ALARM_PIN,INPUT);
  attachInterrupt(digitalPinToInterrupt(ALARM_PIN),alarm,RISING);
  // Start Serial
  Serial.begin(115200);
  // Function to be exposed
  rest.function("getAlarm",getAlarm);
  rest.function("getLight",getLight);
  //rest.function("setAlarmEnable",setAlarmEnable);
  rest.function("getTemperature",getTemperature);
   
  // Give name and ID to device
  rest.set_id("002");
  rest.set_name("Sensor_Station");
   
  // Bridge startup
  Bridge.begin();

  // Listen for incoming connection only from localhost
  server.begin();
  pinMode(13,OUTPUT);
  digitalWrite(13,HIGH);
}

void loop() {
  YunClient client = server.accept();
  rest.handle(client);
}


/*int setAlarmEnable(String command){
  server.println("HTTP/1.1 200 OK\r\n");
  if(command.length() > 0){
    alarmEnable = 1;
  }
  else alarmEnable = 0;
  return alarmEnable;
}*/

int getAlarm(String command){
  server.println("HTTP/1.1 200 OK\r\n");
  bool alarm = wasAlarm;
  wasAlarm = false;
  return alarm;
}

int getTemperature(String command){
  server.println("HTTP/1.1 200 OK\r\n");
  return analogRead(TEMP_PIN);
}

int getLight(String command){
  server.println("HTTP/1.1 200 OK\r\n");
  //server.println("Content-type: application/json\r\n");
  //server.println("Content-Length: 77\r\n");
  return 1-digitalRead(LIGHT_PIN);
}
